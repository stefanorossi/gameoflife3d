# README #

#Requirement:
This program run with an old arduino version. You must install the right version of each sw otherwise it wont work. 

[Processing 2.2.2](https://processing.org/download/) (look for the correct versione)

Libraries:

[G4P 3.5.4](https://sourceforge.net/projects/g4p/files/?source=navbar) (look for the correct version)

[Processing 2.2.2](https://processing.org/download/) 

Extract both libraries in a separate folder into sketchbook libraries folder:
Default libraries folder:

<user>\Documents\Processing\libraries

You can now open processing and run the project.

#Version 1.0

* Always wonder how Conway's game of life would worn in a 3D environnment? now you can!

* 3D interface (rotate, zoom, and explode)

* Settings GUI to change GOL3D rules run-time

* Play/Pause the simulation

* Color based on future cell state

#Installation and run

Clone the repo, install both libraries and open with processing 2.2.2

#Description

Start the project, you can move the camera with your mouse

![Scheme](GoL3D1/images/3d.png)

You can change game rules and view settings 

![Scheme](GoL3D1/images/menu.png)

Rules settings transaltion:

If alive:

- dies with less than <x> neighbours

- dies with more than <x> neighbours

If dead:

- dies with more than <x> neighbours

- dies with less than <x> neighbours

#License 


All my work is released under [DBAD](https://www.dbad-license.org/) license.