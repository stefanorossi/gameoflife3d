public class Cellula{
  private int x, y, z; //posizione nella matriche
  private boolean living, willlive;
  
  
  public Cellula(int x, int y, int z){
    this.x = x;
    this.y = y;
    this.z = z;
    
    living = false;
    willlive = false;
  }
  
  public boolean isLiving(){
    return living;
  }
  
  public boolean getWilllive(){
    return willlive;
  }
  
  public void setLiving(boolean living){
    this.living = living;
  }
  
  public void setWilllive(boolean willlive){
    this.willlive = willlive;
  }
  
  public int[] getCoords(){
    int[] pos = { x, y, z};
    return pos;
  }
}
